//
//  PacketTunnelProvider.swift
//  KeepersWebFiltering
//
//  Created by Omarj on 22/04/2021.
//  Copyright © 2021 Keepers. All rights reserved.
//

import NetworkExtension
import KeepersWebFilteringSDK

class PacketTunnelProvider: NEPacketTunnelProvider {

    let temp = KeepersTunnelProvider()

    override func startTunnel(options: [String : NSObject]?, completionHandler: @escaping (Error?) -> Void) {
        // Add code here to start the process of connecting the tunnel.
        temp.staring(parents: self, options: options, completionHandler: completionHandler)

    }
    
    override func stopTunnel(with reason: NEProviderStopReason, completionHandler: @escaping () -> Void) {
        // Add code here to start the process of stopping the tunnel.
        temp.end(with: reason, completionHandler: completionHandler)

        //completionHandler()
    }
    
    override func handleAppMessage(_ messageData: Data, completionHandler: ((Data?) -> Void)?) {
        // Add code here to handle the message.
        if let handler = completionHandler {
            handler(messageData)
        }
    }
    
    override func sleep(completionHandler: @escaping () -> Void) {
        // Add code here to get ready to sleep.
        completionHandler()
    }
    
    override func wake() {
        // Add code here to wake up.
    }
}
