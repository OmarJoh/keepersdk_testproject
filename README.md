Keepers SDK Sample App
====================
##
SDK official Document URL: (https://keepers-ios-sdk.readme.io/)
##
Sample Application Requirements
##
Requirements
============

1- Mac OS 10.0 and higher.
2- Xcode 11.6 and higher
3- iPhone with iOS 11.0 and higher
4- Apple Developer Account.
5- Sample app project folder

Steps to Run the Project
==================
##
Steps to run the Sample Application
##
1- Connect the iPhone to the Mac using a lightning cable. (charger cable)
##
2- Open Xcode -> Open -> Select Sample Project.
##
3- Select the target device that you connected to your Mac.
##
4- Under Signing and Capabilities Make sure to select the developer account Team you belong to. (if you did not login before you will see the option to add developers account)
##
5- Press Run Button or Command+R and wait until the app being installed on your device.
##
