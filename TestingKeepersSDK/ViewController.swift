//
//  ViewController.swift
//  SDKTestingByQA
//
//  Created by Keepers on 12/08/2020.
//  Copyright © 2020 Keepers. All rights reserved.
//

import UIKit
import CoreLocation
import KeepersChildSDK

class ViewController: UIViewController, CLLocationManagerDelegate, LocationSentToServerDelegate { // SDK location delegate
    
    //MARK:- IBOutlets
    // indecator
    @IBOutlet var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet var loadingIndicatorContainer: UIView!
    
    @IBOutlet var textView: UITextView!
    
    @IBOutlet var containerStackView: UIStackView!
    @IBOutlet var ActionsStackView: UIStackView!
    @IBOutlet var signUpStackView: UIStackView!
    
    @IBOutlet var signUpBtn: UIButton!
    @IBOutlet var getConfigurationBtn: UIButton!
    @IBOutlet var sendLocationBtn: UIButton!
    @IBOutlet var updateDeviceBtn: UIButton!
    @IBOutlet var cancelDeviceBtn: UIButton!
    
    @IBOutlet var AllButtons: [UIButton]!
    //
    let locationManager = CLLocationManager()
    //
    let notificationCenter = UNUserNotificationCenter.current()
    //
    let SDKManager = KeepersSDKSharedManager.shared
    //
    // Sign up Parameters
    var deviceActivationKey = "" // Should be the Magic code
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This location delegate will give feedback when ever the SDK update the server with user location
        SDKManager.locationRequestDelegate = self
        
        // Ask for VPN permission + setup VPN --> this should be called after passing the bundleID in AppDelegate
        startAnimatingLoadingIndicator()
        SDKManager.setupVPN { (didSuccess, error) in
            if didSuccess
            {
                print("didSuccess: " + didSuccess.description)
                
            }else
            {
                print(error.debugDescription)
            }
            self.stopAnimatingLoadingIndicator()// stop regardless failed or success
        }
        //
        locationManager.delegate = self
        
        if UserDefaults.standard.bool(forKey: "userIsSignedIn") == false {
            //user is not signed in - activate the signup button
            showSignUpBtn(true)
        } else {
            //user is signed in - activate the action buttons
            showSignUpBtn(false)
        }
        self.textView.text = ""
        self.textView.layer.borderColor = UIColor.gray.cgColor
        self.textView.layer.borderWidth = 1
        //set to light mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
    }
        
    // SDK Location Delegate
    func locationUpdated() {
        //LocationSentToServerDelegate method
        DispatchQueue.main.async {
            self.updateTextWith("\nChild Location sent to server" + "\n\n------------------------------\n")
        }
    }
    
    // By shaking the device u will be able to share the saved logs
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake{
//            SDKManager.deleteLogs()
            shareText(text: SDKManager.getLogs())
        }
    }
    
    // MARK:- UI Methods
    func showSignUpBtn(_ show: Bool) {
        if show {
            self.ActionsStackView.alpha = 0.5
            self.signUpStackView.alpha = 1.0
        } else {
            self.ActionsStackView.alpha = 1.0
            self.signUpStackView.alpha = 0.5
        }
        self.ActionsStackView.isUserInteractionEnabled = !show
        self.signUpStackView.isUserInteractionEnabled = show
    }
    
    func actionSucceeded(action: NotificationName, _ succeeded: Bool = true) {
        switch action {
        case .CANCEL_DEVICE:
            changeBtnBorderColor(btn: cancelDeviceBtn, succeeded: succeeded)
        case .CONFIGURATION:
            changeBtnBorderColor(btn: getConfigurationBtn, succeeded: succeeded)
        case .REFRESH_LOCATION:
            changeBtnBorderColor(btn: sendLocationBtn, succeeded: succeeded)
        case .SEND_DEVICE_DATA:
            changeBtnBorderColor(btn: updateDeviceBtn, succeeded: succeeded)
        default:
            break
        }
    }
    
    func shareText(text:String){
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func getFeatureName(fromName: NotificationName) -> String {
        switch fromName {
        case .CANCEL_DEVICE:
            return "'Cancel Device'"
        case .CONFIGURATION:
            return "'Get Configuration'"
        case .REFRESH_LOCATION:
            return "'Send Location'"
        case .SEND_DEVICE_DATA:
            return "'Update Device Data'"
        default:
            return ""
        }
    }
    
    func changeBtnBorderColor(btn: UIButton, succeeded: Bool) {
        btn.layer.borderWidth = 2
        if succeeded {
            btn.layer.borderColor = UIColor.green.cgColor
        } else {
            btn.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    func startAnimatingLoadingIndicator() {
        self.loadingIndicatorView!.startAnimating()
        self.loadingIndicatorContainer!.isHidden = false
    }
    
    func stopAnimatingLoadingIndicator() {
        self.loadingIndicatorView!.stopAnimating()
        self.loadingIndicatorContainer!.isHidden = true
        
    }
    
    func updateTextWith(_ txt: String) {
        self.textView.text += txt
        let bottom = NSMakeRange(textView.text.count - 1, 1)
        textView.scrollRangeToVisible(bottom)
    }
    
    func alertWithTF() {
        //Step : 1
        let alert = UIAlertController(title: "Required Field", message: "Please input the child activation key", preferredStyle: UIAlertController.Style.alert )
        //Step : 2
        let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if textField.text != "" {
                //Read TextFields text data
                print(textField.text!)
                print("TF 1 : \(textField.text!)")
                self.deviceActivationKey = textField.text!
            } else {
                print("TF 1 is Empty...")
                self.deviceActivationKey = ""
            }

        }

        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "activation key"
            textField.textColor = .red
        }
        

        //Step : 4
        alert.addAction(save)
        //Cancel action
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
        alert.addAction(cancel)
        //OR single line action
        //alert.addAction(UIAlertAction(title: "Cancel", style: .default) { (alertAction) in })

        self.present(alert, animated:true, completion: nil)

    }
    //MARK:- Signup Simulation
    @IBAction func signUpClicked(_ sender: Any) {
        if deviceActivationKey == ""{
            alertWithTF()
        }else{
            //ask for permissions: notification, location
            if UserDefaults.standard.bool(forKey: "askedForPermissions") == false {
                askForPermissionsAndSignup()
            } else{
                signup()
            }
        }
       
    }
    
    //MARK:- Notification Simulation
    @IBAction func getConfigClicked(_ sender: Any) {
        let str = "{\"configuration_id\": \"-1\", \"notificationId\":\"" +  NotificationName.CONFIGURATION.rawValue + "\"}"
        handleNotification(.CONFIGURATION, payload: str)
    }
    
    @IBAction func sendLocationClicked(_ sender: Any) {
        handleNotification(.REFRESH_LOCATION, payload: "{\"notificationId\":\"" +  NotificationName.REFRESH_LOCATION.rawValue + "\"}")
    }
    
    @IBAction func updateDeviceClicked(_ sender: Any) {
        handleNotification(.SEND_DEVICE_DATA, payload: "{\"notificationId\":\"" +  NotificationName.SEND_DEVICE_DATA.rawValue + "\"}")
    }
    
    @IBAction func cancelDeviceClicked(_ sender: Any) {
        handleNotification(.CANCEL_DEVICE, payload: "{\"notificationId\":\"" +  NotificationName.CANCEL_DEVICE.rawValue + "\"}")
    }
    
    @IBAction func clearTvClicked(_ sender: Any) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Erase all Text", message: "Are you sure?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(alert: UIAlertAction!) in
                self.textView.text = ""
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
}

//MARK:- Helper Methods
extension ViewController{
    func signup() {
        //
        DispatchQueue.main.async {
            self.startAnimatingLoadingIndicator()
        }
        //
        SDKManager.childSignUp(deviceActivationKey: deviceActivationKey){requestStatus, missingValue, statusCode in
            
            switch requestStatus{
            //
            case .Succeed:
                DispatchQueue.main.async {
                    self.cancelDeviceBtn.layer.borderWidth = 0
                    self.textView.text += "signup succeeded with status code \(String(describing: statusCode ?? 0))\nIf you didn't get the configuration via a push notification, click 'Get Configuration'."
                    //user is signed in - activate the action buttons
                    self.showSignUpBtn(false)
                    //remember user signin status
                    UserDefaults.standard.set(true, forKey: "userIsSignedIn")
                    UserDefaults.standard.synchronize()
                    self.changeBtnBorderColor(btn: self.signUpBtn, succeeded: true)
                }
            //
            case .Missing_Value:
                DispatchQueue.main.async {
                    self.changeBtnBorderColor(btn: self.signUpBtn, succeeded: false)
                    self.textView.text += "Failed to sign in, missing "
                }
                
                if missingValue == SignupMissingValue.Missing_Location_Permission {

                    DispatchQueue.main.async {
                        self.textView.text += "location permission"
                        print("location permission")
                        let alert = UIAlertController(title: "Missing Permission", message: "Please accept the missing permission", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                    }
                    
                }else if missingValue == SignupMissingValue.Missing_Cloud_Token || missingValue == SignupMissingValue.Missing_Notification_Permission {
                    
                    DispatchQueue.main.async {
                        self.textView.text += "notification permission"
                        let alert = UIAlertController(title: "Missing Permission", message: "Please accept the notifications permission", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                    }
                    
                } else if missingValue == SignupMissingValue.Missing_VPN_Permission {
                    DispatchQueue.main.async {
                        self.textView.text += "VPN permission"
                    }
                }
            //
            case .Failed, .Error_With_ErrorCode:
                
                DispatchQueue.main.async {
                    self.changeBtnBorderColor(btn: self.signUpBtn, succeeded: false)
                    self.textView.text += "Failed to sign up."
                    if statusCode != nil {
                        self.textView.text += " status code: \(String(describing: statusCode!))"
                    }
                }
            case .No_Internet:
                DispatchQueue.main.async {
                    self.changeBtnBorderColor(btn: self.signUpBtn, succeeded: false)
                    self.updateTextWith("Failed to sign up, No internet connection available.")
                }
            //
            default:
                break
                
            }
            //
            DispatchQueue.main.async {
                self.textView.text += "\n\n-------------------------------------------\n"
                self.stopAnimatingLoadingIndicator()
            }
        }
    }
    
    func handleNotification(_ notificationName: NotificationName, payload: String) {
        DispatchQueue.main.async {
            self.startAnimatingLoadingIndicator()
        }
        let feature = getFeatureName(fromName: notificationName)
        do{
            if let json = payload.data(using: String.Encoding.utf8){
                if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                    self.SDKManager.handleNotification(notification: jsonData){ requestStatus, statusCode in
                        
                        switch requestStatus{
                        
                        case .Succeed:
                            // For example if succeeded canceling device
                            DispatchQueue.main.async {
                                self.updateTextWith("\n" + feature + " succeeded, with status code \(String(describing: statusCode ?? 0))")
                                if notificationName == .CANCEL_DEVICE {
                                    self.showSignUpBtn(true)
                                    UserDefaults.standard.set(false, forKey: "userIsSignedIn")
                                    UserDefaults.standard.synchronize()
                                    
                                    self.AllButtons.forEach({$0.layer.borderWidth = 0})
                                }
                                self.actionSucceeded(action:  notificationName)
                            }
                        case .Feature_Disabled:
                            DispatchQueue.main.async {
                                self.updateTextWith(feature + " failed, feature disabled")
                                self.actionSucceeded(action: notificationName, false)
                            }
                        case .Failed, .Error_With_ErrorCode:
                            DispatchQueue.main.async {
                                self.updateTextWith(feature + " failed.")
                                self.actionSucceeded(action: notificationName, false)
                                if statusCode != nil {
                                    self.updateTextWith(" error code: \(String(describing: statusCode!))")
                                }
                            }
                        case .No_Need_To_Update_Based_On_Configuration:
                            DispatchQueue.main.async {
                                self.updateTextWith(feature + " succeeded")
                                self.actionSucceeded(action: notificationName, true)
                            }
                        case .No_Internet:
                            DispatchQueue.main.async {
                                self.actionSucceeded(action: notificationName, false)
                                self.updateTextWith("\(feature) Failed, No internet connection available.")
                            }
                        default:
                            break
                        }
                        DispatchQueue.main.async {
                            self.stopAnimatingLoadingIndicator()
                            self.updateTextWith("\n\n-------------------------------------------\n")
                        }
                    }
                    
                }
            }
        }catch {
            print(error.localizedDescription)
            
        }
        
    }//end of method handleNotification
}

// MARK:- Permissions Requst
//permissions extension
extension ViewController {
    /**
     Ask for needed permissions.
     */
    func askForPermissionsAndSignup() {
        registerForPushNotifications()
    }
    
    /**
     Register for push notification.
     */
    func registerForPushNotifications(){
        notificationCenter.requestAuthorization(options: [.alert]) {
            [weak self] granted, error in
            if granted {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            if CLLocationManager.authorizationStatus() != .notDetermined {
                //location permission has been asked for before.
                UserDefaults.standard.set(true, forKey: "askedForPermissions")
                UserDefaults.standard.synchronize()

            } else {
                UserDefaults.standard.set(true, forKey: "clickedPopUpPermission")
                UserDefaults.standard.synchronize()

                self!.requestLocationPermission()
                
            }
            
        }
        
    }
    
    func requestLocationPermission() {
        locationManager.requestAlwaysAuthorization()
    }
    
    /**
     Handle locatoin authorization changed.
     */
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var txt = "\nLocation Permission: "
        let authorizationStatus : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        switch authorizationStatus {
        case CLAuthorizationStatus.authorizedAlways:
            txt += "authorizedAlways"
        case CLAuthorizationStatus.notDetermined:
            txt += "notDetermined"
        case CLAuthorizationStatus.authorizedWhenInUse:
            txt += "authorizedWhenInUse"
        case CLAuthorizationStatus.restricted:
            txt += "restricted"
        case .denied:
            txt += "denied"
        @unknown default:
            txt += "default"
        }
        
        if authorizationStatus != .notDetermined {
            UserDefaults.standard.set(true, forKey: "askedForPermissions")
            UserDefaults.standard.synchronize()

        }
        if UserDefaults.standard.bool(forKey: "clickedPopUpPermission") {
            UserDefaults.standard.set(false, forKey: "clickedPopUpPermission")
            UserDefaults.standard.synchronize()

            self.signup()
        }
    }
    
}
