//
//  AppDelegate.swift
//  SDKTestingByQA
//
//  Created by Keepers on 12/08/2020.
//  Copyright © 2020 Keepers. All rights reserved.
//

import UIKit
import KeepersChildSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    // Step #1
    // This is where the SDK Manager should be initilize
    let SDKmanager = KeepersSDKSharedManager.shared
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //SDKmanager.log("didFinishLaunchingWithOptions")
        
        
        SDKmanager.setAppGroup(to: "group.com.keepers.KeepersVPN-MVP")
        
        SDKmanager.setNetworkExtensionBundleID(to: "com.keepers.vodafoneStage.KeepersVNP-Ext")
        
        SDKmanager.deleteAllFiles()
        //
        // Step #2 asking for access token
        UIApplication.shared.registerForRemoteNotifications()
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
       // SDKmanager.log("applicationWillTerminate")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
      //  SDKmanager.log("applicationDidBecomeActive")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {

    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {

    }
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
  /* "timestamp" : "1203131230",
    "traceId" : "c3d03f94cbd1-px2n7-siw25", "deviceId": "13a2c012-0b39-4d92-a661-40103abf08be", "configuration_id": "41", "notificationId": "CONFIGURATION"
    //MARK:- Notifications*/
    public func userNotificationCenter(_ center: UNUserNotificationCenter,willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        
        guard let userInfo = notification.request.content.userInfo as? [String:Any] else { return }
        let notif = userInfo["aps"]
        if (notif as? [String : Any]).self != nil {
            // Step #4.1
            //SDK will handle received notification.
            SDKmanager.handleNotification(notification: notif as! [AnyHashable : Any]){_,_ in }
            completionHandler([.alert, .badge, .sound])
        }
        else {print("App received notification that dosn't belongs to iOS users") ; return }
    }
    
    // when success register for remote notificataion
    public func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //send token to SDK
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("register to push notif, token = \(token) ")
        print("device token = \(deviceToken)")
        // Step #3
        // Passing the Device token to the SDK
        SDKmanager.setRemoteNotificationToken(deviceToken: deviceToken)
    }
    
    public func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    
    // This method will be invoked even if the application was launched or resumed because of the remote notification.
    public func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {
        // Step #4.2
            SDKmanager.handleNotification(notification: userInfo){_,_ in }
            completionHandler(.newData)
    }
}

