//
//  Utility.swift
//  SDKTestingByQA
//
//  Created by Keepers on 13/08/2020.
//  Copyright © 2020 Keepers. All rights reserved.
//

import Foundation

enum NotificationName : String {

    case REFRESH_LOCATION       = "action_refresh_location"
    case CANCEL_DEVICE          = "action_cancel_device"
    case CONFIGURATION          = "CONFIGURATION"
    case SEND_DEVICE_DATA       = "action_send_device_data"
    
    case NOT_FOUND              = "NAME_NOT_FOUND"
    //for testing in external sdk app
    init(_ rawValue: String) {
        self = NotificationName(rawValue:rawValue) ?? .NOT_FOUND
    }
}
